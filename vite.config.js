import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      includeAssets: ['icon_app_48x48.png', 'icon_app_192x192.png'],
      manifest: {
        name: 'Culqi App',
        short_name: 'Culqi',
        description: 'Aplicacion de pagos',
        theme_color: '#ffffff',
        icons: [
          {
            src: 'icons/icon_app_48x48.png',
            sizes: '48x48',
            type: 'image/png'
          },
          {
            src: 'icons/icon_app_72x72.png',
            sizes: '72x72',
            type: 'image/png'
          },
          {
            src: 'icons/icon_app_96x96.png',
            sizes: '96x96',
            type: 'image/png'
          },
          {
            src: 'icons/icon_app_144x144.png',
            sizes: '144x144',
            type: 'image/png'
          },
          {
            src: 'icons/icon_app_192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
        ]
      },
      workbox:{
        runtimeCaching: [{
          urlPattern: ({url}) =>{
            return url.pathname.startsWith('bun-burn-env.eba-ftyx2m3h.us-east-1.elasticbeanstalk')
          },
          handler: 'CacheFirst',
          options: {
            cacheName: 'api-cache',
            cacheableResponse: {
              statuses: [0,200,201]
            }
          }
        }]
      }
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
