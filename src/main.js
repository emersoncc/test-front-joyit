import { createSSRApp } from 'vue'

import App from './App.vue'
import store from './store'
import router from './router'

import myIconComponent from './components/myIconComponent.vue'

import './assets/main.css'

router.beforeEach( async (to, from, next) => {
  const needAuth = to.meta.requireAuth
  if (needAuth) {
    const authTokenUser = sessionStorage.getItem('authToken')
    const userLoggedStatus = sessionStorage.getItem('userLogged')
    if (authTokenUser !== null & authTokenUser !== undefined & authTokenUser !== '' & (userLoggedStatus === 'true' || userLoggedStatus === true)) {
      store.commit('setAppTitle', to.meta.title)
      next()
    } else {
      next('/no-auth')
    }
  } else {
    store.commit('setAppTitle', to.meta.title)
    next()
  }
})

const app = createSSRApp(App)
  app.component('myIconComponent', myIconComponent)
  app.use(store)
  app.use(router)
  app.mount('#app')

/* export function createApp() {
  const app = createSSRApp(App)
  app.component('myIconComponent', myIconComponent)
  app.use(store)
  app.use(router)
  app.mount('#app')
  return { app }
} */