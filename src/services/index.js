import axios from 'axios'
const urlLocalBase = window.location.origin + '/'
const urlRestApiBase = import.meta.env.VITE_API_BASE_URL

const getUserAuthService  = () => {
  var pomise = new Promise((resolve, reject) => {
    axios.get(urlLocalBase + 'mocks/GET.userAuthService.json').then((result) => {
      setSessionUser(result.data.data.token).then((res) => {
        resolve(res)
      }).catch((err) => {
        const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
      })
    }).catch((err) => {
      const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
    })
  })
  return pomise
}

const getUserBalance  = () => {
  const appToken = sessionStorage.getItem('authToken')
  var pomise = new Promise((resolve, reject) => {
    axios.get(urlRestApiBase + '/getActualBalance', { 
      headers: {
        Authorization: `Bearer ${appToken}`
      }}).then((result) => {
      resolve(result.data)
    }).catch((err) => {
      const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
    })
  })
  return pomise
}

const getCompanyAvailableList  = () => {
  const appToken = sessionStorage.getItem('authToken')
  var pomise = new Promise((resolve, reject) => {
    axios.get(urlRestApiBase + '/getProviders', { headers: { Authorization: `Bearer ${appToken}` } }).then((result) => {
      resolve(result.data)
    }).catch((err) => {
      const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
    })
  })
  return pomise
}

const getUserWalletCode  = () => {
  var pomise = new Promise((resolve, reject) => {
    axios.get(urlLocalBase + 'mocks/GET.userWalletCode.json').then((result) => {
      resolve(result.data)
    }).catch((err) => {
      const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
    })
  })
  return pomise
}

const getPaymentAvailableServices  = () => {
  var pomise = new Promise((resolve, reject) => {
    axios.get(urlLocalBase + 'mocks/GET.paymentsAndServices.json').then((result) => {
      resolve(result.data)
    }).catch((err) => {
      const errorData = {
        code: err.response.status,
        message: err.message
      }
      reject(errorData)
    })
  })
  return pomise
}

const setSessionUser  = (token) => {
  return new Promise((resolve, reject) => {
    try {
      sessionStorage.setItem('userLogged', true)
      sessionStorage.setItem('authToken', token)
      resolve('OK')
    } catch (e) {
      reject('ERROR')
    }
  })
}

const destroySessionUser  = () => {
  return new Promise((resolve, reject) => {
    try {
      sessionStorage.removeItem('userLogged')
      sessionStorage.removeItem('authToken')
      resolve('OK')
    } catch (e) {
      reject('ERROR')
    }
  })
}

const GeneralService = {
  getUserAuthService,
  getUserBalance,
  getUserWalletCode,
  getPaymentAvailableServices,
  getCompanyAvailableList,
  setSessionUser,
  destroySessionUser
}

export default GeneralService
