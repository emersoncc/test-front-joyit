import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import PaymentsAndRechargeView from '../views/PaymentsAndRechargeView.vue'
import CompanySearchView from '../views/CompanySearchView.vue'
import NotFoundView from '../views/NotFoundView.vue'
import NoAuthView from '../views/NoAuthView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'loginPage',
      meta: {
        title: null,
        requireAuth: false,
      },
      component: LoginView
    },
    {
      path: '/payments',
      name: 'paymentsPage',
      meta: {
        title: 'Recargas y pagos',
        requireAuth: true,
      },
      component: PaymentsAndRechargeView
    },
    {
      path: '/mobile-top-up',
      name: 'mobileTopUpPage',
      meta: {
        title: 'Recargas',
        requireAuth: true,
      },
      component: CompanySearchView
    },
    {
      path: '/no-auth',
      name: 'notAuthPage',
      meta: {
        title: 'No autorizado',
        requireAuth: false,
      },
      component: NoAuthView
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notFoundPage',
      meta: {
        title: 'Página no encontrada',
        requireAuth: false,
      },
      component: NotFoundView
    }
  ]
})


export default router
