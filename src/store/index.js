import { createStore } from "vuex"
import { iconsList } from '../assets/icons'

let formatCurrency = Intl.NumberFormat("es-PE", {
  style: "currency",
  currency: "PEN",
})

export default createStore({
  state: {
    //App Header
    appTitle: null,

    // User Balance
    userBalance: '',

    // User Balance
    userWalletCode: '',

    // Payment Services List
    paymentAvailableServices: [],

    // Company List
    companyAvailableList: [],

    //Local Icons
    myIcons: iconsList
  },

  getters: {
    userBalanceFormated(state) {
      return state.userBalance == '' ? '' : formatCurrency.format(state.userBalance)
    }
  },

  mutations: {
    async setUserBalance(state, value) {
      state.userBalance = value
    },
    async setUserWalletCode(state, value) {
      state.userWalletCode = value
    },
    async setPaymentServicesList(state, value) {
      state.paymentAvailableServices = value
    },
    async setCompanyList(state, value) {
      state.companyAvailableList = value
    },
    async setCompanyFavorite(state, value) {
      state.companyAvailableList[value].favorite = !state.companyAvailableList[value].favorite
    },
    async setAppTitle(state, value) {
      state.appTitle = value
    },
  },

  actions: {},
  modules: {},
})
